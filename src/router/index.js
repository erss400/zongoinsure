import Vue from 'vue'
import Router from 'vue-router'
import Welcome from '@/components/Welcome'
import SignIn from '@/components/account/SignIn'
import SignUp from '@/components/account/SignUp'
import Verification from '@/components/account/Verify'
import UHome from '@/components/account/user/Home'
import CWelcome from '@/components/account/company/Welcome'
import Home from '@/components/account/Home'
import GetStarted from '@/components/account/company/GetStarted'
import Settings from '@/components/account/company/Settings'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'Welcome',
      component: Welcome,
      meta: {
        user: true
      }
    },
    {
      path: '/login',
      name: 'SignIn',
      component: SignIn,
      meta: {
        forVisitors: true
      }
    },
    {
      path: '/register',
      name: 'SignUp',
      component: SignUp,
      meta: {
        forVisitors: true
      }
    },
    {
      path: '/verify',
      name: 'Verification',
      component: Verification,
      meta: {
        forVisitors: true
      }
    },
    {
      path: '/:username/home',
      name: 'Home',
      component: Home,
      meta: {
        forAuth: true
      }
    },
    {
      path: '/:username/get-started',
      name: 'GetStarted',
      component: GetStarted,
      meta: {
        forAuth: true
      }
    },
    {
      path: '/settings',
      name: 'Settings',
      component: Settings,
      meta: {
        forAuth: true
      }
    },
    {
      path: '/company',
      name: 'CWelcome',
      component: CWelcome,
      meta: {
        company: true
      }
    },
  ],
  mode: 'history',
  linkActiveClass: 'active'
})
